<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryCreateRequest;
use App\Http\Requests\CategoryEditRequest;
use Illuminate\Http\Request;
use App\Models\Shop;

class ShopController extends Controller
{
    public function index()
    {
        $shops = Shop::get()->toTree();
        return view('shop', compact ('shops'));
    }

    public function create(Request $request)
    {
        if ($request->act == "Добавить категорию") {
            return view('actions.createCategory');
        } else if ($request->act == "Добавить товар") {
            $shops = Shop::all();
            return view('actions.createGoods', compact('shops'));
        } else {
            return back();
        }
    }

    public function createGoodsForCurrentCategory($value)
    {
        $currentCategory = Shop::find($value);
        $shops = Shop::all();
        return view('actions.createGoods', compact('currentCategory', 'shops'));
    }

    public function store(CategoryCreateRequest $request)
    {
        $shop = Shop::create($request->all());
        return redirect(route('shop.index'));
    }

    public function show(Shop $shop)
    {
        return view('actions.show', compact('shop'));
    }

    public function edit(Shop $shop, CategoryEditRequest $request)
    {
        $shop->update($request->all());

        return redirect(route('shop.index'));
    }

    public function destroy(Shop $shop)
    {
        $shop->delete();
        return redirect(route('shop.index'));
    }

    public function openCategory(Shop $shop)
    {
        $shops = Shop::all();
        $currentCategory = Shop::find($shop->id);
        if (Shop::where('parent_id', $shop->id)->count() != 0) {
            return view('actions.openCategory', compact('shop'));
        } else {
            return view('actions.createGoods', compact('currentCategory', 'shops'));
        }
    }
}