<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Shop extends Model
{
    protected $fillable = [
        'category_name',
        'parent_id',
    ];
    use NodeTrait;
}
