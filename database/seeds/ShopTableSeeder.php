<?php

use Illuminate\Database\Seeder;
use App\Models\Shop;

class ShopTableSeeder extends Seeder
{
    public function run()
    {
        $shops = [
            [
                'category_name' => 'Фрукты',
                    'children' => [
                        [
                            'category_name' => 'Цитрусовые',
                            'children' => [
                                    ['category_name' => 'Лимон'],
                                    ['category_name' => 'Апельсин'],
                                    ['category_name' => 'Грейпфрут'],
                            ],
                        ],
                        [
                            'category_name' => 'Розоцветные',
                                'children' => [
                                    ['category_name' => 'Яблоко'],
                                    ['category_name' => 'Слива'],
                                    ['category_name' => 'Земляника'],
                            ],
                        ],
                        [
                            'category_name' => "Тропические",
                                'children' => [
                                    ['category_name' => 'Папайя'],
                                    ['category_name' => 'Банан'],
                                    ['category_name' => 'Абрикос'],
                            ]
                        ]
                    ],
                ],
                [
                    'category_name' => 'Овощи',
                        'children' => [
                        [
                            'category_name' => 'Пряные',
                            'children' => [
                                ['category_name' => 'Укроп'],
                                ['category_name' => 'Базилик'],
                            ],
                        ],
                        [
                            'category_name' => 'Клубнеплодные',
                            'children' => [
                                ['category_name' => 'Картофель'],
                                ['category_name' => 'Топинамбур'],
                                ['category_name' => 'Батат'],
                            ],
                        ],
                    ],
                ],
        ];
        foreach($shops as $shop)
        {
            Shop::create($shop);
        }
    }
}