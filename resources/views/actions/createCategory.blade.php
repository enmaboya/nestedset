@extends('includes.layout')

@section('content')
    <form action = "{{ route ('shop.store') }}" method = "POST">
        @csrf
        <div class="input-group-prepend">
        <span class="input-group-text">Добавление категории товаров: </span>
        </div>
        <div style="width: 300px">
            <input type="text" aria-label="Название" class="form-control" placeholder="Укажите название" name="category_name">
            <div><button class="btn btn-outline-success" type="submit">Добавить</button></div>
        </div>
    </form>
@endsection