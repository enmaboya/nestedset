@extends('includes.layout')

@section('content')
    <form action = "{{ route ('shop.store') }}" method = "POST">
        @csrf
        <div class="input-group-prepend">
        <span class="input-group-text">Что хотите добавить?</span>
        </div>
        <div style="width: 300px">
            <div>
                <select class="custom-select" id="inputGroupSelect01" name="parent_id">
                    <option disabled selected>Выберите категорию</option>
                    @foreach ($shops as $shop)
                        @isset($currentCategory)
                        <option selected value="{{$currentCategory->id}}"> {{$currentCategory->category_name}} </option>
                        @endisset
                        <option value="{{$shop->id}}"> {{$shop->category_name}} </option>
                    @endforeach
                </select>
            </div>
            <input type="text" aria-label="Название" class="form-control" placeholder="Укажите название" name="category_name">
            <div><button class="btn btn-outline-success" type="submit">Добавить</button></div>
        </div>
    </form>
@endsection