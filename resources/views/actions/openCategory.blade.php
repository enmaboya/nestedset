@extends('includes.layout')

@section('addvar')
    @include('includes.secondadd')
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('category', $shop) }}
@endsection
@section('content')
    @foreach($shop->children as $category)
        <div class="col-md-4">
            <div class="row">
                <form action="{{route ('shop.show', $category) }}" method="get">
                    <button class="btn btn-outline-success" type="submit">✎</button>
                </form>
                <form action="{{route ('shop.destroy', $category) }}" method="post">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-outline-danger">⮾</button>
                </form>
                <h4><a class="nav-item nav-link" href="{{ route('shop.openCategory', $category)}}">&nbsp; {{ $category->category_name }}</a></h4>
            </div>
            <hr />
        </div>
    @endforeach
@endsection