@extends('includes.layout')

@section('content')
  <table class="table table-bordered">
      <thead>
        <tr>
          <th scope="col" width="">Данные: </th>
        </tr>
      </thead>
      <tbody>
        <tr  width="120">
          <form action = "{{route ('shop.edit', $shop) }}" method = "GET">
            <td width="400">
              <div>
                <div>
                  <label for="disabledTextInput">Название</label>
                  <input type="text" value="{{$shop->category_name}}" placeholder="{{$shop->category_name}}" name="category_name">
                </div>
              <div class="input-group-prepend">
                <button class="btn btn-outline-info" type="submit">Принять изменения</button></div>
              </div>
            </td>
          </form>
        </tr>
      </tbody>
  </table>
@endsection