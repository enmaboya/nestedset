<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Вложенные данные</title>
    @include('includes.head')
</head>
<body>
    <div class="container">
        <div class="card">
            <div class="card-body">
                @include('includes.navbar')
            </div>
        </div>
    </div>
    <main class="py-4">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    @yield('content')
                </div>
            </div>
        </div>
    </main>
</body>
</html>