@extends('includes.layout')

@section('addvar')
    @include('includes.mainadd')
@endsection

@section('breadcrumb')
    {{ Breadcrumbs::render('shop') }}
@endsection
@section('content')
    @foreach($shops as $category)
    <div class="col-md-12">
        <div class="row">
            <h3> <a class="nav-item nav-link" href="{{ route('shop.openCategory', $category) }}">{{ $category->category_name }} </a>&nbsp; </h3>
                <form action="{{route ('shop.show', $category) }}" method="get">
                    <button class="btn btn-outline-success" type="submit">✎</button>
                </form>
                <form action="{{route ('shop.destroy', $category) }}" method="post">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-outline-danger">⮾</button>
                </form>
        </div>
        <hr />
    </div>
    @endforeach
@endsection