<?php

// Home
Breadcrumbs::for('shop', function ($bread) {
    $bread->push('Главная', route('shop.index'));
});

Breadcrumbs::for('value', function ($bread, $category) {
    $bread->push($category->category_name, route('shop.openCategory', $category));
});

//Category
Breadcrumbs::for('category', function ($bread, $category) {
    if ($category->parent) {
        $bread->parent('category', $category->parent);
    } else {
        $bread->parent('shop');
    }
    $bread->push($category->category_name, route('shop.openCategory', $category));
});